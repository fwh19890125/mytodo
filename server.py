#coding=utf-8
from __future__ import absolute_import

import json
import os
import web
from jinja2 import Environment, FileSystemLoader
from model import Todos

RUN_PATH = os.path.abspath(os.path.dirname(__file__))
TEMPLATE_PATH = os.path.join(RUN_PATH, 'static/html')

loader = FileSystemLoader(TEMPLATE_PATH)
lookup = Environment(loader=loader)
        
urls = (
	'/','index',
	'/todo','todo',
	'/todo/(\d+)','todo',
	'/todos/','todos',
     '/static/(.*)', 'hello',
     '/login','login',
     '/logout','logout'
)


class index:
    """docstring for index"""
    def GET(self):
        #return 'Welcome Home!'
        if not session.login:
            return web.Found("/login")
        t = lookup.get_template('index.html')
        return t.render()

'''class todo:
	"""docstring for todo"""

	def GET(self,id):
		context={
			"title":"吃饭",
			"order":0,
			"done":False,
		}
		return json.dumps(context,ensure_ascii=False).decode("utf-8").encode("gbk")
class todos:
    def GET(self):
        result = []
        result.append({
            "title": "下午3点,coding",
            "order": 0,
            "done": False,
        })
        return json.dumps(result,ensure_ascii=False).decode("utf-8").encode("gbk")
        '''
class todo:
    def GET(self, todo_id=None):
        result = None
        todo = Todos.get_by_id(id=todo_id)
        result = {
            "id": todo.id,
            "title": todo.title,
            "order": todo._order,
            "done": todo.done == 1,
        }
        return json.dumps(result)

    def POST(self):
        data = web.data()
        todo = json.loads(data)
        # 转换成_order, order是数据库关键字, sqlite3报错
        todo['_order'] = todo.pop('order')
        Todos.create(**todo)

    def PUT(self, todo_id=None):
        data = web.data()
        todo = json.loads(data)
        todo['_order'] = todo.pop('order')
        Todos.update(**todo)

    def DELETE(self, todo_id=None):
        Todos.delete(id=todo_id)

class todos:
    def GET(self):
        todos = []
        itertodos = Todos.get_all()
        for todo in itertodos:
            todos.append({
                "id": todo.id,
                "title": todo.title,
                "order": todo._order,
                "done": todo.done == 1,
            })
        return json.dumps(todos)

class hello:        
    def GET(self, name):
        if not name: 
            name = 'World'
        return 'Hello, ' + name + '!'
class login:
    def GET(self):
        t=lookup.get_template('login.html')
        return t.render()
    def POST(self):
        user,pwd=web.input().get("user"),web.input().get("pass")
        if user and user==pwd:
            session.login=True
            return web.Found("/")
        t=lookup.get_template("login.html")
        return t.render({"error":u"用户名或密码错误"})
class logout:
    def GET(self):
        session.login=False
        t=lookup.get_template("login.html")
        return t.render()

app = web.application(urls, globals())
from web.httpserver import StaticMiddleware
application = app.wsgifunc(StaticMiddleware)

if web.config.get('_session') is None:
    session = web.session.Session(
        app,
        web.session.DiskStore('sessions'),
        initializer={'login': False}
    )
    web.config._session = session
else:
    session = web.config._session

def main():
    app.run()

if __name__ == "__main__":
    main()