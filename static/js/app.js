(function($){
	
	var Todo = Backbone.Model.extend({
		urlRoot:"/todo",
		defaults:function(){
			return {
				title:"empty todo...",
				done:false,
				order:todos.nextOrder()
			};
		},
		toggle:function(){
			this.save({done: !this.get("done")});
		}
	});
	var TodoList = Backbone.Collection.extend({
		url:"/todos/",
		model:Todo,
		//localStorage: new Backbone.LocalStorage("todos-backbone"),
		done:function(){
			return this.where({done:true});
		},
		remaining:function(){
			return this.where({done:false});
		},
	    nextOrder: function() {
	    	if (!this.length) return 1;
	    	return this.last().get('order') + 1;
	    },
	    comparator: 'order'
	});
	var todos=new TodoList;
	var TodoView = Backbone.View.extend({
		tagName:"li",
		template: _.template($("#tpl_item").html()),
		initialize: function(){
			this.listenTo(this.model, 'change', this.render);
      		this.listenTo(this.model, 'destroy', this.remove);
		},
		render:function(){
			this.$el.html(this.template(this.model.toJSON()));
			this.$el.toggleClass("done",this.model.get("done"));
			this.input=this.$(".edit");
			return this;
		},
		events:{
			"click .toggle":"toggleDone",
			"click .del":"deleteItem",
			"dblclick .view":"edit",
			"keypress .edit":"updateOnEnter",
			"blur .edit":"blur"
		},
		toggleDone:function(){
			this.model.toggle();
		},
		deleteItem:function(){
			this.model.destroy();
		},
		edit:function(){
			this.$el.addClass("editing");
			//this.$el.removeClass("done");
			this.input.focus();
		},
		updateOnEnter:function(e){
			if(e.keyCode!=13) return;
			if(!this.input.val()) return;
			this.model.save({title:this.input.val()});
			this.$el.removeClass("editing");
		},
		blur:function(){
			if(!this.input.val()) return;
			this.model.save({title:this.input.val()});
			this.$el.removeClass("editing");
		}
	});

	var AppView = Backbone.View.extend({
		el:$(".todoApp"),
		statusTemplate:_.template($("#tpl_status").html()),
		initialize:function(){
			//alert(1);
			this.input = this.$("#new_todo");
			this.allCheckbox = this.$("#toggle-all")[0];
			this.footer = this.$('.m-ft');
			this.main = $('.m-bd');
			this.listenTo(todos, 'add', this.addOne);
			this.listenTo(todos, 'all', this.render);
			todos.fetch();
			/*this.allCheckbox = this.$("#toggle-all")[0];

			this.listenTo(todos, 'add', this.addOne);
			this.listenTo(todos, 'reset', this.addAll);
			this.listenTo(todos, 'all', this.render);

			this.footer = this.$('footer');
			this.main = $('#main');

			todos.fetch();*/
		},
		render:function(){
			var done = todos.done().length;
			var remaining = todos.remaining().length;

			if (todos.length) {
				this.main.show();
				this.footer.show();
				this.footer.html(this.statusTemplate({done: done, remaining: remaining}));
			} else {
				this.main.hide();
				this.footer.hide();
			}

			this.allCheckbox.checked = !remaining;
		},
		events:{
			"keypress #new_todo": "createOnEnter",
			"click #toggle-all":"toggleAllComplete",
			"click #clear-completed":"clearCompleted"
		},
		createOnEnter:function(e){
			//alert(1);
			if (e.keyCode != 13) return;
			if (!this.input.val()) return;
			//alert(this.input.val());

			todos.create({title: this.input.val()});
			
			this.input.val('');
			//this.addOne({title: this.input.val()});
		},
		addOne:function(todo){
			//alert("add one");
			var view = new TodoView({model: todo});
      		this.$(".m-todoList").append(view.render().el);
		},
		toggleAllComplete:function(){
			var done=this.allCheckbox.checked;
			todos.each(function(todo){
				todo.save({done:done});
			});
		},
		clearCompleted:function(){
			/*todos.each(function(todo){
				if(todo.get("done")){
					todo.destroy();
				}
			});*/
			//通过sql查询的方式删除
			_.invoke(todos.done(), 'destroy');
			return false;
		}
	});
	var appView=new AppView;
})(jQuery);